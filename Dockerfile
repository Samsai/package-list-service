FROM clux/muslrust as build-stage

COPY . /usr/src/backend

WORKDIR /usr/src/backend

RUN cargo clean
RUN cargo build --release

FROM alpine

COPY --from=build-stage /usr/src/backend /usr/src/backend
WORKDIR /usr/src/backend

CMD ["./target/x86_64-unknown-linux-musl/release/package-list-service"]
