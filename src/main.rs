#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;
#[macro_use] extern crate rocket_cors;
#[macro_use] extern crate serde;
#[macro_use] extern crate serde_derive;
extern crate regex;

use rocket_contrib::json::{Json, JsonValue};
use rocket::State;
use rocket::response::Redirect;

use rocket::http::Method;
use rocket_cors::{AllowedHeaders, AllowedOrigins, Error};

use std::sync::RwLock;
use std::env;

mod package_parser;

use package_parser::*;

#[get("/api/package/<name>")]
fn package(package_list: State<RwLock<PackageList>>, name: String) -> Option<Json<Package>> {
    let mut packages = package_list.read().unwrap();

    if let Some(package) = packages.get_package(&name) {
        return Some(Json(package.clone()));
    } else {
        return None;
    }
}

#[get("/api/refresh")]
fn refresh_packages(package_list: State<RwLock<PackageList>>) -> Redirect {
    let mut packages = package_list.write().unwrap();

    if let Ok(real_data) = read_dpkg_data() {
        packages.parse_list(&real_data);
    } else if let Ok(mock_data) = mock_data_file() {
        packages.parse_list(&mock_data);
    }

    return Redirect::to(uri!(index));
}

#[get("/api")]
fn index(package_list: State<RwLock<PackageList>>) -> JsonValue {
    let mut packages = package_list.read().unwrap();

    return json!({ "packages": packages.get_package_names() });
}

fn main() -> Result<(), rocket_cors::Error> {
    let frontend_url = env::var("PKGLIST_FRONTEND").unwrap_or("http://localhost:3000".to_string());

    println!("Using {} as frontend...", frontend_url);

    // You can also deserialize this
    let cors = rocket_cors::CorsOptions {
        allowed_origins: AllowedOrigins::some_exact(&[frontend_url]),
        allowed_methods: vec![Method::Get].into_iter().map(From::from).collect(),
        allowed_headers: AllowedHeaders::some(&["Authorization", "Accept"]),
        allow_credentials: false,
        ..Default::default()
    }
    .to_cors()?;

    rocket::ignite().manage(RwLock::new(PackageList::new()))
                    .mount("/", routes![index, package, refresh_packages])
                    .attach(cors)
                    .launch();

    Ok(())
}
