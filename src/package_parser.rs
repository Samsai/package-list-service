
use regex::Regex;

use std::collections::HashMap;
use std::collections::HashSet;
use std::fs::File;
use std::io::prelude::*;

/// Package storage container
pub struct PackageList {
    packages: HashMap<String, Package>,
}

impl PackageList {
    /// Create a new PackageList
    pub fn new() -> PackageList {
        return PackageList { packages: HashMap::new() };
    }

    /// Parse a package list from a string slice
    pub fn parse_list(&mut self, string: &str) {
        // Temporary storage for reverse dependencies
        let mut reverse_dependencies: HashMap<String, Vec<String>> = HashMap::new();

        // Split the string by empty lines to get separate packages
        let package_strings: Vec<&str> = string.split("\n\n").collect();

        // Parse each individual package
        for pkg_string in package_strings {
            // Can the current string be parsed into a package?
            if let Some(new_package) = parse_package(pkg_string) {
                // Collect reverse dependencies
                for dependency in &new_package.dependencies {
                    if let Some(list) = reverse_dependencies.get_mut(dependency) {
                        list.push(String::from(new_package.name.clone()));
                    } else {
                        reverse_dependencies.insert(dependency.clone(), vec![new_package.name.clone()]);
                    }
                }

                // Add new package to the list
                self.packages.insert(new_package.name.clone(), new_package);
            }

        }

        // Update reverse dependencies for each package
        for (name, reverse_dependencies) in reverse_dependencies {
            if let Some(package) = self.packages.get_mut(&name) {
                package.reverse_dependencies = reverse_dependencies;
            }
        }
    }

    pub fn get_package(&self, name: &str) -> Option<&Package> {
        return self.packages.get(name);
    }

    /// Get all the package names on the list
    ///
    /// NOTE: This function makes an effective copy of all package names
    pub fn get_package_names(&self) -> Vec<String> {
        let mut list: Vec<String> = self.packages.keys().map(|s| s.clone()).collect();
        list.sort();

        return list;
    }
}

/// Representation of a Package
#[derive(Deserialize, Serialize, Clone)]
pub struct Package {
    pub name: String,
    pub description: String,
    pub dependencies: HashSet<String>,
    pub reverse_dependencies: Vec<String>
}

/// Parse a single package from a string slice
pub fn parse_package(string: &str) -> Option<Package> {
    let name = parse_name(string)?;
    let description = parse_description(string)?;
    let dependencies = parse_dependencies(string);

    return Some(Package { name, description, dependencies, reverse_dependencies: Vec::new() });
}

/// Parse a package name from a string slice
fn parse_name(string: &str) -> Option<String> {
    let regex_name = Regex::new(r"Package: (.*)").unwrap();

    if let Some(name_match) = regex_name.captures(string) {
        return name_match.get(1).map(|m| String::from(m.as_str()));
    } else {
        return None;
    }
}

/// Parse a description from a string slice
fn parse_description(string: &str) -> Option<String> {
    // Match all lines starting from "Description: " that start with space
    // or until the text ends
    let regex_description = Regex::new(r"Description:(( .*(\n|$))+)").unwrap();

    if let Some(description_match) = regex_description.captures(string) {
        return description_match.get(1)
                                .map(|m| m.as_str())
                                .map(|m| m.replace(" .\n", "\n\n"))
                                .map(|m| String::from(m));
    } else {
        return None;
    }
}

/// Parse the dependencies of a package from a string slice
fn parse_dependencies(string: &str) -> HashSet<String> {
    let regex_dependencies = Regex::new(r"Depends: (.*)").unwrap();

    if let Some(matches) = regex_dependencies.captures(string) {
        let dependencies: Vec<&str> = matches.get(1)
                                             .unwrap()
                                             .as_str()
                                             .split(", ")
                                             .collect();

        // Strip versioning from dependencies
        return dependencies.iter().map(|s| {
            let parts: Vec<&str> = s.split(' ').collect();

            String::from(parts[0])
        }).collect();
    } else {
        return HashSet::new();
    }
}

pub fn mock_data_file() -> Result<String, std::io::Error> {
    let mut file = File::open("mock-data.txt")?;
    let mut contents = String::new();

    file.read_to_string(&mut contents)?;

    return Ok(contents);
}

pub fn read_dpkg_data() -> Result<String, std::io::Error> {
    let mut file = File::open("/var/lib/dpkg/status")?;
    let mut contents = String::new();

    file.read_to_string(&mut contents)?;

    return Ok(contents);
}

pub fn mock_data() -> &'static str {
    r#"
Package: python-pkg-resources
Status: install ok installed
Priority: optional
Section: python
Installed-Size: 175
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Architecture: all
Source: distribute
Version: 0.6.24-1ubuntu1
Replaces: python2.3-setuptools, python2.4-setuptools
Provides: python2.6-setuptools, python2.7-setuptools
Depends: python (>= 2.6), python (<< 2.8)
Suggests: python-distribute, python-distribute-doc
Conflicts: python-setuptools (<< 0.6c8-3), python2.3-setuptools (<< 0.6b2), python2.4-setuptools (<< 0.6b2)
Description: Package Discovery and Resource Access using pkg_resources
 The pkg_resources module provides an API for Python libraries to
 access their resource files, and for extensible applications and
 frameworks to automatically discover plugins.  It also provides
 runtime support for using C extensions that are inside zipfile-format
 eggs, support for merging packages that have separately-distributed
 modules or subpackages, and APIs for managing Python's current
 "working set" of active packages.
Original-Maintainer: Matthias Klose <doko@debian.org>
Homepage: http://packages.python.org/distribute
Python-Version: 2.6, 2.7"#
}


pub fn mock_data_2() -> &'static str {
    r#"
Package: ubuntu-standard
Status: install ok installed
Priority: optional
Section: metapackages
Installed-Size: 57
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Architecture: amd64
Source: ubuntu-meta
Version: 1.267
Depends: at, busybox-static, cpio, cron, dmidecode, dnsutils, dosfstools, ed, file, ftp, hdparm, info, iptables, language-selector-common, logrotate, lshw, lsof, ltrace, man-db, memtest86+, mime-support, parted, pciutils, popularity-contest, psmisc, rsync, strace, time, usbutils, wget
Recommends: apparmor, apt-transport-https, bash-completion, command-not-found, friendly-recovery, iputils-tracepath, irqbalance, manpages, mlocate, mtr-tiny, nano, ntfs-3g, openssh-client, plymouth, plymouth-theme-ubuntu-text, ppp, pppconfig, pppoeconf, tcpdump, telnet, ufw, update-manager-core, uuid-runtime
Description: The Ubuntu standard system
 This package depends on all of the packages in the Ubuntu standard system.
 This set of packages provides a comfortable command-line Unix-like
 environment.
 .
 It is also used to help ensure proper upgrades, so it is recommended that
 it not be removed."#
}


#[cfg(test)]
mod tests {
    extern crate benchmarking;
   
    use super::*;

    #[test]
    fn can_parse_single_package() {
        let package = parse_package(mock_data());

        assert!(package.is_some());
    }


    #[test]
    fn benchmark_package_parsing() {
        benchmarking::warm_up();

        let bench_result = benchmarking::measure_function(|measurer| {
            measurer.measure(move || {
                let package = parse_package(mock_data());
            })
        }).unwrap();

        println!("Parsing single package took: {:?}", bench_result.elapsed());
    }

    #[test]
    fn single_package_name_correct() {
        let package = parse_package(mock_data()).unwrap();

        assert_eq!("python-pkg-resources", &package.name);
    }

    #[test]
    fn single_package_description_correct() {
        let package = parse_package(mock_data()).unwrap();

        assert_eq!("python-pkg-resources", &package.name);
    }

    #[test]
    fn single_package_dependencies_correct() {
        let package = parse_package(mock_data()).unwrap();

        assert!(package.dependencies.contains("python"));
    }

    #[test]
    fn can_read_mock_data() {
        assert!(mock_data_file().is_ok());
    }

    #[test]
    fn can_parse_package_list() {
        let mut packages = PackageList::new();
        packages.parse_list(&mock_data_file().unwrap());

        assert!(packages.packages.len() > 0);
    }

    #[test]
    fn benchmark_package_list_parsing() {
        benchmarking::warm_up();

        let bench_result = benchmarking::measure_function(|measurer| {
            let mut packages = PackageList::new();

            let contents = mock_data_file().unwrap();

            measurer.measure(move || {
                packages.parse_list(&contents);
            })
        }).unwrap();

        println!("Parsing package list took: {:?}", bench_result.elapsed());
    }

    #[test]
    fn package_list_contains_python() {
        let mut packages = PackageList::new();
        packages.parse_list(&mock_data_file().unwrap());

        assert!(packages.packages.contains_key("python"));
    }
}
